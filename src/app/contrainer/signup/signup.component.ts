import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpserviceService } from './../../providers/httpservice.service';
import { CommonDataService } from './../../providers/common-data.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignUpComponent implements OnInit {
  signupForm: FormGroup;
  constructor(private router: Router, private httpService: HttpserviceService, public cds: CommonDataService) {
    this.signupForm = new FormGroup({
      'name': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'phone': new FormControl('', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
      'designation': new FormControl('', [Validators.required]),
      'organisation': new FormControl('', [Validators.required]),
      'website': new FormControl('', [Validators.required]),
      'department': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required]),
      'cpassword': new FormControl('', [Validators.required]),
      'logo': new FormControl('1'),
      'primary_job_category': new FormControl('1')
    });
  }
  numberOnly(event:any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  onSubmit() {
    let self = this;
    var reqData = this.signupForm.value
    if (reqData.password == reqData.cpassword) {
      delete reqData.cpassword;
      this.httpService.postWithoutAuth("recruiters/register", reqData).subscribe((res: any) => {
        if (res.type) {
          sessionStorage.setItem("userTocken", res.data.jwt);
          localStorage.setItem("userInfo", JSON.stringify(res.data));
          self.httpService.showSuccess(res.message);
          self.router.navigateByUrl("/home");
        }
        else {
          if (res.message) {
            if (res.message.message != undefined)
              self.httpService.showError(res.message.message);
            else
              self.httpService.showError(res.message);
          }
        }
      });
    }
    else {
      this.httpService.showError("Password and Confirm password does not matched.");
    }
  }
  ngOnInit(): void {
    if (this.cds.isLoggedIn())
      this.router.navigateByUrl('/jobs/dashboard');
  }

}
