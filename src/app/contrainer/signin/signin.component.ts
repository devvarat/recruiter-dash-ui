import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonDataService } from 'src/app/providers/common-data.service';
import { HttpserviceService } from 'src/app/providers/httpservice.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SignInComponent implements OnInit {
  loginForm : FormGroup;
  constructor(private router: Router, public httpService: HttpserviceService,public cds:CommonDataService) {
    this.loginForm=new FormGroup({
    'email': new FormControl('', [Validators.pattern(/^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))|(\d{10})$/)]), 
    'password': new FormControl('', [Validators.required])
  });
 }
 submitLogin(){
   let self=this;
  this.httpService.postWithoutAuth("recruiters/login",this.loginForm.value).subscribe((res:any)=>{
    if(res.type){
      localStorage.setItem("userTocken",res.token);
      localStorage.setItem("userInfo",JSON.stringify(res.data));
      self.httpService.auth_token=res.token
      self.httpService.userInfo=res.data
      self.httpService.showSuccess(res.message);
      self.router.navigateByUrl("/jobs/dashboard"); 
   }
     else {
       if(res.message) {
         this.httpService.showError(res.message);
      }
    }
  })

 }
  ngOnInit(): void {
    if(this.cds.isLoggedIn())
    this.router.navigateByUrl('/jobs/dashboard');
  }

}
