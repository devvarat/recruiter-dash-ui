
import { NgModule, Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { MaterialModule } from './material-module';
import { HeaderComponent } from '../component';
// import { ThemeComponent } from './@theme.component';
import { SignInComponent,SignUpComponent } from '../contrainer';
import { HomepageComponent } from '../homepage/homepage.component';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { AddWorkExperienceComponent } from '../components/add-work-experience/add-work-experience.component';
import { AddRemarksPopoupComponent } from '../components/add-remarks-popoup/add-remarks-popoup.component';
import { ScheduleInterviewPopoupComponent } from '../components/schedule-interview-popoup/schedule-interview-popoup.component';
import { SaveProjectDialogComponent } from '../components/save-project-dialog/save-project-dialog.component';
import { SaveCertificateDialogComponent } from '../components/save-certificate-dialog/save-certificate-dialog.component';
import { NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';

// Component Declare

export const ThemeRoutes: Routes = [
  {
    path: 'home',
    component: HomepageComponent,
  }, {
    path: 'sign-in',
    component: SignInComponent,
  }, {
    path: 'sign-up',
    component: SignUpComponent,
  },
  {
    path: 'homepage',
    component:HomepageComponent,
  },




];

const BASE_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  MatIconModule,
  HttpClientModule,
  MaterialModule,
  NgxMatTimepickerModule
];
const COMMON_COMPONENTS = [HeaderComponent, SignInComponent,SignUpComponent,HomepageComponent,
  AddWorkExperienceComponent,
  AddRemarksPopoupComponent,
  ScheduleInterviewPopoupComponent,
  SaveProjectDialogComponent,
  SaveCertificateDialogComponent,
];
// const COMPONENTS = [ThemeComponent];

@NgModule({
  imports: [
    ...BASE_MODULES,
    RouterModule.forChild(ThemeRoutes),
    MDBBootstrapModulesPro.forRoot(),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [...COMMON_COMPONENTS/* , ...COMPONENTS */],
  //   providers: [CommonService,VimeoService],
    exports:[...COMMON_COMPONENTS,...BASE_MODULES]
})

export class ThemeModule { }
