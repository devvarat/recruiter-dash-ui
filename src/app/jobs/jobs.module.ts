import { NgModule } from '@angular/core';

import { JobsRoutingModule } from './jobs-routing.module';
import { JobsComponent } from './jobs.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ThemeModule } from '../@theme/@theme.module';
import { SaveJobComponent } from './save-job/save-job.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { ProfileComponent } from './profile/profile.component';
import { SaveEducationDialogComponent } from '../components/save-education-dialog/save-education-dialog.component';
@NgModule({
  declarations: [
    JobsComponent,
    DashboardComponent,
    SaveJobComponent,
    ProfileComponent,
    SaveEducationDialogComponent
  ],
  imports: [
    ThemeModule,
    JobsRoutingModule,
    CarouselModule
  ]

  
})
export class JobsModule { }
