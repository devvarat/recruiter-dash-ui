import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { AddWorkExperienceComponent } from 'src/app/components/add-work-experience/add-work-experience.component';
import { SaveEducationDialogComponent } from 'src/app/components/save-education-dialog/save-education-dialog.component';
import { ScheduleInterviewPopoupComponent } from 'src/app/components/schedule-interview-popoup/schedule-interview-popoup.component';
import { HttpserviceService } from './../../providers/httpservice.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {
  @Input() pageContent: any;
  selectedCandidateFilter: any = { status: "", sortType: "" }
  constructor(public dialog: MatDialog, private sanitizer: DomSanitizer, public httpService: HttpserviceService) {

  }
  openAddEducationDialog() {
    const dialogRef = this.dialog.open(SaveEducationDialogComponent, { restoreFocus: false, width: '60vw', minWidth: "320px" });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  addworkexperience() {
    const dialogRef = this.dialog.open(AddWorkExperienceComponent, { restoreFocus: false, width: '60vw', minWidth: "320px" });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  updateInterViewStatus(item: any) {
    let self = this;
    console.log(item)
<<<<<<< HEAD
=======
    let userInfo = JSON.parse(localStorage.getItem("userInfo")!);
>>>>>>> dev
      var interviewDetails = {
        "title": "",
        "job_id": this.pageContent.selectedjobId,
        "job_category_id": "",
        "description": "",
        "recruiter_id":userInfo._id,
        "candidate_id": item.user,
        "sector_id": this.pageContent.selectedFilter.sector,
        "job_description_doc": "",
        "salary": "",
        "start_time": "",
        "remark": item.statusDetails.remark,
        "interview_date": "",
        "interview_status": item.statusDetails.interview_status,
        "status": true
      }
<<<<<<< HEAD
      this.httpService.postWithoutAuth("interviews", interviewDetails).subscribe((res: any) => {
        if (res.type) {
          self.httpService.showSuccess(res.message);
        }
      });
=======
      this.fetchJobDetails(this.pageContent.selectedjobId).subscribe((res: any) => {
        interviewDetails.job_category_id = res.job_category_id
        interviewDetails.recruiter_id = res.recruiter_id
        interviewDetails.salary = res.salary
        let self = this;
        this.httpService.postWithoutAuth("interviews", interviewDetails).subscribe((res: any) => {
          console.log(res);
          if (res.type) {
            self.httpService.showSuccess("Candidate successfully shortlisted, go to candidates tab to see shortlist.");
          }
        });
      });
  }
  fetchJobDetails(jobId: any) {
    return this.httpService.getWithoutAuth("jobs/" + jobId);
>>>>>>> dev
  }
  scheduleInterviewPopup(item: any): void {
    let self = this;
    const dialogRef = this.dialog.open(ScheduleInterviewPopoupComponent, {
      minWidth: '320px', maxWidth: '40%', data: { job_id: this.pageContent.selectedjobId, sector_id: this.pageContent.selectedFilter.sector, candidate: item },
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log(result);
      // self.fetchApplications()
    });
  }


  ngOnInit(): void {
    console.log(this.pageContent);
   /*  setTimeout(() => {
      let self = this;
      if (self.pageContent?.videoResume)
        self.pageContent.videoResume = self.sanitizer.bypassSecurityTrustResourceUrl(this.pageContent?.videoResume + '&rel=0')
      console.log(self.pageContent.videoResume);

    }, 500); */
    // this.pageContent.videoResume=this.sanitizer.bypassSecurityTrustResourceUrl(this.pageContent?.videoResume+'&rel=0')
  }

}
