import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonDataService } from 'src/app/providers/common-data.service';
import { HttpserviceService } from 'src/app/providers/httpservice.service';
interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-save-job',
  templateUrl: './save-job.component.html',
  styleUrls: ['./save-job.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class SaveJobComponent implements OnInit {
  slug:any;
  signupForm: FormGroup;
  jobCategoriesList: any = [];
  sectoresList: any = [];
  imageChangedEvent: any = '';
  fileTypeProfile = ''
  base64textString:any;
  uploadedImagesData:any;
  
  constructor(private route: ActivatedRoute,private router: Router, public httpService: HttpserviceService, public cds: CommonDataService) {
    this.route.params.subscribe(param => {
      this.slug= param.id;
      if(this.slug)
      this.getJobDetails()
    //  console.log(this.slug);
   });
  let userInfo=JSON.parse(localStorage.getItem("userInfo")!);
  this.signupForm = new FormGroup({
    'title': new FormControl('', [Validators.required]),
    'job_category_id': new FormControl(''),
    'sector_id': new FormControl('', [Validators.required]),
    'description': new FormControl('', [Validators.required]),
    'recruiter_id': new FormControl(userInfo._id),
    'address': new FormControl('', [Validators.required]),
    'job_description_doc': new FormControl(''),
    'salary': new FormControl('', [Validators.required]),
    'type': new FormControl('', [Validators.required]),
    'valid_till': new FormControl(''),
    'job_status': new FormControl('hiring', [Validators.required]),
    'status': new FormControl('1'),
  });
  this.fetchJobCategories();
  this.fetchSectors()
}
fetchSectors() {
  let self = this;
  this.httpService.getL4o("category?categoryType=sector").subscribe((res: any) => {
    self.sectoresList = res.categoryList;
    // console.log(self.sectoresList);
  });
}

getFile(evt:any) {
  const file = evt.target.files[0];
  //   // console.log(file);
  //   // console.log(file.type);
  this.fileTypeProfile = file.type;
  this.imageChangedEvent = evt;
  if (file) {
    this.handleReaderLoaded(file)
    /* const reader = new FileReader();
    reader.onload = this.handleReaderLoaded.bind(this);
    reader.readAsBinaryString(file); */

  }
}
handleReaderLoaded(e:any) {
  this.base64textString = e.target.files[0];
  const formData = new FormData();
  formData.append("attachment", this.base64textString);
  // formData.append("source", 'profile');
  /* if (this.base64textString != null) {
    formData.append("image", this.base64textString);
  } */
  /* const data={
    "image": this.base64textString
  } */
  this.httpService.uploadFile(formData).subscribe((data: any) => {
    if (data.type) {
      this.uploadedImagesData = data.name;
    }
  })
}
getJobDetails(){
  let self = this;
  this.httpService.getWithoutAuth("jobs/"+this.slug).subscribe((res: any) => {
    self.signupForm.patchValue(res);
    // console.log(self.jobCategoriesList);
  });  
}
fetchJobCategories() {
  let self = this;
  this.httpService.getWithoutAuth("job_categories").subscribe((res: any) => {
    self.jobCategoriesList = res;
    console.log(self.jobCategoriesList);
  });
}
deleteJob(){
  if(confirm("Are you sure to Delete this Job?")){
    let self = this;
    let formData=this.signupForm.value;
    let url;
    if(this.slug){
      url="jobs/update";
      formData._id=this.slug
      formData.status='0'
    }
    
    this.httpService.postWithoutAuth(url,formData ).subscribe((res: any) => {
      if (res.type) {
        self.httpService.showSuccess(res.message);
        this.router.navigateByUrl("/jobs");
      }
      else {
        if (res.message) {
          this.httpService.showError(res.message);
        }
      }
    });
  }
}
onSubmit() {
  let self = this;
  let formData=this.signupForm.value;
  if(this.uploadedImagesData)
    formData.job_description_doc=this.uploadedImagesData;
  let url;
  if(this.slug){
    url="jobs/update";
    formData._id=this.slug
  }
  else
    url="jobs";
  
  this.httpService.postWithoutAuth(url,formData ).subscribe((res: any) => {
    if (res.type) {
      self.httpService.showSuccess(res.message);
      this.router.navigateByUrl("/jobs");
    }
    else {
      if (res.message) {
        this.httpService.showError(res.message);
      }
    }
  });
}

  ngOnInit(): void {
  }

}
