import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleInterviewPopoupComponent } from './schedule-interview-popoup.component';

describe('ScheduleInterviewPopoupComponent', () => {
  let component: ScheduleInterviewPopoupComponent;
  let fixture: ComponentFixture<ScheduleInterviewPopoupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleInterviewPopoupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleInterviewPopoupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
