import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRemarksPopoupComponent } from './add-remarks-popoup.component';

describe('AddRemarksPopoupComponent', () => {
  let component: AddRemarksPopoupComponent;
  let fixture: ComponentFixture<AddRemarksPopoupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddRemarksPopoupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRemarksPopoupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
