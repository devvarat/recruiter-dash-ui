import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-save-education-dialog',
  templateUrl: './save-education-dialog.component.html',
  styleUrls: ['./save-education-dialog.component.scss']
})
export class SaveEducationDialogComponent implements OnInit {
  frmData:any={
  degree: "Graduate",
  endDate: "",
  institute: "",
  specialization: "",
  startDate: "",
  edit:false
  }
  constructor(public dialogRef: MatDialogRef<SaveEducationDialogComponent>,@Inject(MAT_DIALOG_DATA) public data:any) {
    if(this.data.edit)
      this.frmData=data
    console.log(this.frmData);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
  }

}
